#!/usr/bin/env python
"""Tests for `oso_measurements` package."""
# pylint: disable=redefined-outer-name
from oso_measurements import __version__
from oso_measurements.oso_measurements_interface import GreeterInterface
from oso_measurements.oso_measurements_impl import HelloWorld

def test_version():
    """Sample pytest test function."""
    assert __version__ == "0.0.1"

def test_GreeterInterface():
    """ testing the formal interface (GreeterInterface)
    """
    assert issubclass(HelloWorld, GreeterInterface)

def test_HelloWorld():
    """ Testing HelloWorld class
    """
    hw = HelloWorld()
    name = 'yvain'
    assert hw.greet_the_world(name) == f"Hello world, {name} !"

