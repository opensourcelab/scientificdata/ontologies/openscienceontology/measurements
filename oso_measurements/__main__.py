#!/usr/bin/env python3
# vim:fileencoding=utf-8
"""_____________________________________________________________________

:PROJECT: OSO measurements ontology

* Main module command line interface *

:details:  Main module command line interface. 
           !!! Warning: it should have a diffent name than the package name.

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

"""Main module implementation. !!! Warning: it should have a diffent name than the package name. """
"""Console script for oso_measurements."""

import argparse
import sys
import logging
from .__init__ import __version__

from .oso_measurements_impl import HelloWorld

def parse_command_line():
    """ Looking for command line arguments"""

    description = "oso_measurements"
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("_", nargs="*")

    parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + __version__)

    # add more arguments here

    return parser.parse_args()

def main():
    """Console script for oso_measurements."""
        # or use logging.INFO (=20) or logging.ERROR (=30) for less output
    logging.basicConfig(
        format='%(levelname)-4s| %(module)s.%(funcName)s: %(message)s', level=logging.DEBUG)
    
    
    args = parse_command_line()
        
    if len(sys.argv) <= 2:
        logging.debug("no arguments provided !")


    print("Arguments: " + str(args._))
    print("Replace this message by putting your code into oso_measurements.__main__")
    return 0


if __name__ == "__main__":
    logging.basicConfig(
        format="%(levelname)-4s| %(module)s.%(funcName)s: %(message)s",
        level=logging.DEBUG,
    )
    
    hw = HelloWorld()
    greeting = hw.greet_the_world("yvain")
    logging.debug(greeting)

    sys.exit(main())  # pragma: no cover
