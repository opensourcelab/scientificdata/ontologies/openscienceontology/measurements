#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""_____________________________________________________________________

:PROJECT: OSO - Open Science Ontology

* OSO measurements implementation *

:details:  Main module implementation.

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

import os
import logging

from ontopy import World
from ontopy.utils import write_catalog

import owlready2

__version__ = "0.0.2"  # Version of this ontology


# --- helper functions 

def en(s):
    """Returns `s` as an English location string."""
    return owlready2.locstr(s, lang='en')


def pl(s):
    """Returns `s` as a plain literal string."""
    return owlready2.locstr(s, lang='')


class OSO_Measurement:
    def __init__(self) -> None:

        self.oso_measurement_base_iri = 'http://www.oso.org/oso/measurements#'
        self.oso_measurement_version_iri = f'http://www.oso.org/{__version__}/oso/measurements'

        output_filename_base = os.path.join('..', '..','oso_measurement')
        self.oso_measurement_owl_filename = f'{output_filename_base}-v{__version__}.owl'
        self.oso_measurement_ttl_filename = f'{output_filename_base}-v{__version__}.ttl'

        # Load crystallography, which imports emmo
        # alternative url   "https://raw.githubusercontent.com/emmo-repo/EMMO/master/self.emmo.ttl"

        self.emmo_url = (
                'https://raw.githubusercontent.com/emmo-repo/emmo-repo.github.io/'
                'master/versions/1.0.0-beta/emmo-inferred-chemistry2.ttl')
        #self.world = World(filename="emmo_measurements.sqlite3")
        self.world = World()
        self.emmo = self.world.get_ontology(self.emmo_url)
        self.emmo.load()
        self.emmo.sync_python_names()  # Syncronize annotations
        self.emmo.base_iri = self.emmo.base_iri.rstrip('/#')
        self.catalog_mappings = {self.emmo.base_iri: self.emmo_url}

        # Create new ontology
        self.oso = self.world.get_ontology(self.oso_measurement_base_iri)
        self.oso.imported_ontologies.append(self.emmo)
        self.oso.sync_python_names()

        
    def define_ontology(self):
        logging.debug('defining ontology')

        with self.oso:
            
            # Basic Relations
            # ================

            class hasType(oso.hasConvention):
                """Associates a type (string, number...) to a property."""

            class isTypeOf(oso.hasConvention):
                """Associates a property to a type (string, number...)."""
                inverse_property = hasType

            # Properties
            # ==========

            class Duration(oso.Time):
                """Duration of a measurement
                """

                is_a = [
                    oso.hasReferenceUnit.only(
                        oso.hasPhysicalDimension.only(oso.Time)
                    ),
                    hasType.exactly(1, oso.Real),
                ]

            class Wavelength(oso.Length):
                """The Wavelength is the spatial period of a periodic wave—the distance over which the wave's shape repeats"""

                is_a = [
                    oso.hasReferenceUnit.only(
                        oso.hasPhysicalDimension.only(oso.Length)
                    ),
                    hasType.exactly(1, oso.Real),
                ]

            class Absorbance(oso.RatioQuantity):
                """The absorbance of a beam of collimated monochromatic radiation of Electro-Magnetic radiation in a homogeneous isotropic medium is proportional to the 
                absorption path length and to the concentration of the absorbing species. Absorbance is a logarithmic measure of the amount of 
                light (at a specific wavelength) that is absorbed when passing through a sample.  A= -log10(I/I0), where A is absorbance, I is 
                the intensity of incident light and I0 is the intensity of light transmitted after passing through the sample."""

                wikipediaEntry = "https://en.wikipedia.org/wiki/Absorbance"

            class Fluorescence(oso.RatioQuantity):
                """A combined process of photon absorption by an atom or molecule followed by spontaneous photon emission.."""

            class QuantumYield(oso.RatioQuantity):
                """The quantum yield (Φ) of a radiation-induced process is the number of times a specific event occurs per photon absorbed by the system."""

            class MaterialFlux(oso.DerivedQuantity):
                """ """
                is_a = [
                    oso.hasReferenceUnit.some(
                        oso.hasPhysicalDimension.only(oso.VolumeDimension),
                        # oso.hasPhysicalDimension.only(oso.Time)
                    ),
                    hasType.exactly(1, oso.Real),
                ]

            #  Relations
            # ===========

            class hasDuration(oso.hasConvention, oso.Measurement >> Duration, owlready2.FunctionalProperty):
                """ Associates a duration to a measurement
                """

            # Measurement Classes
            # ====================

            # Basic ------

            class KineticMeasurement(oso.Measurement):
                """Measurement of quantities under varying time."""

                is_a = [oso.hasProperty.some(oso.Time)]

            class SpectroscopicMeasurement(oso.Measurement):
                """Measurement of quantities under varying interactions."""

            class LengthMeasurement(oso.Measurement):
                """ Mesurement of the Length of matter
                """
                is_a = [oso.hasProperty.some(oso.Length)]

            class VolumeMeasurement(oso.Measurement):
                """ Mesurement of the Volume of matter
                """
                is_a = [oso.hasProperty.some(oso.Volume)]

            # Mass ---

            class MassMeasurement(oso.Measurement):
                """ Mesurement of the Mass of matter
                """
                is_a = [oso.hasProperty.some(oso.Mass)]

            class MassSpectroscopicMeasurement(oso.Measurement):
                """Measurement separation of a mixture of matter based on their mass."""

            class ForceMeasurement(oso.Measurement):
                """Measurement of a Force
                """

                is_a = [oso.hasProperty.some(oso.Force)]

            # AFM / AFS

            # Acceleration

            class PressureMeasurement(oso.Measurement):
                """Measurement of a pressure
                """

                is_a = [oso.hasProperty.some(oso.Pressure)]

            # Thermodynamic ------------

            class TemperatureMeasurement(oso.Measurement):
                """ Mesurement of the Thermodynamic Temperature of matter
                """
                is_a = [oso.hasProperty.some(oso.ThermodynamicTemperature)]

            # heat capacity
            # heat transfer
            # caliometry

            # Electromagnetic ----------------

            class ElectricCurrentMeasurement(oso.Measurement):
                """Measurements of the Electric Current
                """
                is_a = [oso.hasProperty.some(oso.ElectricCurrent)]

            class ElectricPotentialMeasurement(oso.Measurement):
                """Measurements of the Electric Potential
                """
                is_a = [oso.hasProperty.some(oso.ElectricPotential)]

            class ElectricConductivityMeasurement(oso.Measurement):
                """Measurements of the Electric Conductivity
                """
                is_a = [oso.hasProperty.some(oso.ElectricConductivity)]

            # resistance, ... dipolemoment, ....

            class ElectromagneticMeasurement(oso.Measurement):
                """Measurement based on interactions of matter with Electro-Magnetic Radiation
                """

            class AbsorbanceMeasurement(ElectromagneticMeasurement):
                """Measurement of the Absorbance of Electro-Magnetic Radiation (like, e.g. of UV-vis, IR light) by some matter at a certain Wave Length."""

                is_a = [oso.hasProperty.some(Absorbance),
                        oso.hasProperty.some(Wavelength)]

            class LuminanceMeasurement(ElectromagneticMeasurement):
                """Measurement of the Luminance. - check if equal to light intensity"""

                is_a = [oso.hasProperty.some(oso.Luminance)]

            class FluorescenceMeasurement(ElectromagneticMeasurement):
                """Measurement of the Fluorescence."""

                is_a = [oso.hasProperty.some(Fluorescence),
                        oso.hasProperty.some(QuantumYield), ]

                # time resolved FL

            class ElectromagneticSpectroscopy(ElectromagneticMeasurement, SpectroscopicMeasurement):
                """ Measurement of the interaction between Electro-Magnetic Readiation and matter as a function of the wavelenth."""

                is_a = [oso.hasProperty.some(Wavelength)]

            class AbsorbanceSpectroscopy(ElectromagneticMeasurement):
                """ Measurement of the interaction between Electro-Magnetic Readiation and matter as a function of the wavelenth."""

                is_a = [oso.hasProperty.some(Absorbance),
                        oso.hasProperty.some(Wavelength)]

            # EPR / ESR
            # NMR

            # Chromatographic measurements ----

            class ChromatographicMeasurement(oso.Measurement):
                """Measurement based on interactions of matter with matter resulting in a separation of matter.
                """

            class GasChromotographicMeasurement(ChromatographicMeasurement):
                """Measurement of the separtion of Gases by the interaction with solid material"""

                is_a = [oso.hasProperty.some(oso.ThermodynamicTemperature),
                        oso.hasProperty.some(MaterialFlux),
                        oso.hasProperty.some(oso.Pressure)]

            # detection - needs flexibilty for new methods

            class LiquidChromotographicMeasurement(ChromatographicMeasurement):
                """Measurement of the separtion of Liquid matter by the interaction with solid material"""

                is_a = [oso.hasProperty.some(oso.ThermodynamicTemperature),
                        oso.hasProperty.some(MaterialFlux),
                        oso.hasProperty.some(oso.Pressure)]

                # detection - needs flexibilty for new methods

            # HPLC

            # class SolidChromotographicMeasurement(ChromatographicMeasurement):

            # IonExchangeChromatography
            # SizeExclusionChromatography

            # Electroporesis
            # GelElectroporesis
            # CapillaryElectroporesis
            # AffinityChromatography

            # Complex measurement -----------

            # Reaction Kinetics
    
    def save_ontology(self, format='turtle'):
        if format == 'turtle':
            logging.debug(f'Saving new ontology as {format} to {self.oso_measurement_ttl_filename} ')
            oso_measurement_filename = self.oso_measurement_ttl_filename

        else:
            logging.debug(f'Saving new ontology as {format} to {self.oso_measurement_owl_filename} ')
            oso_measurement_filename = self.oso_measurement_owl_filename

        self.oso.sync_attributes(name_policy='uuid', class_docstring='elucidation',
                            name_prefix='OSO_')
        
        self.oso.set_version(version_iri=self.oso_measurement_version_iri)
        self.osodir_label = False

        self.catalog_mappings[self.oso_measurement_version_iri] = oso_measurement_filename 

        #################################################################
        # Annotate the ontology metadata
        #################################################################

        self.oso.metadata.abstract.append(en(
                'An EMMO-based domain ontology scientific measurements.'
                'oso-measurement is released under the Creative Commons Attribution 4.0 '
                'International license (CC BY 4.0).'))

        self.oso.metadata.title.append(en('OSO-Measurement'))
        self.oso.metadata.creator.append(en('mark doerr'))
        self.oso.metadata.contributor.append(en('university greifswald'))
        self.oso.metadata.publisher.append(en(''))
        self.oso.metadata.license.append(en(
            'https://creativecommons.org/licenses/by/4.0/legalcode'))
        self.oso.metadata.versionInfo.append(en(__version__))
        self.oso.metadata.comment.append(en(
            'The EMMO requires FaCT++ reasoner plugin in order to visualize all'
            'inferences and class hierarchy (ctrl+R hotkey in Protege).'))
        self.oso.metadata.comment.append(en(
            'This ontology is generated with data from the ASE Python package.'))
        self.oso.metadata.comment.append(en(
            'Contacts:\n'
            'mark doerr\n'
            'University Greifswald\n'
            'email: mark.doerr@suni-greifswald.de\n'
            '\n'
            ))


        self.oso.save(oso_measurement_filename , overwrite=True)
        #self.oso.save(oso_measurement_owl_filename, overwrite=True)
        write_catalog(self.catalog_mappings)
        # self.oso.sync_reasoner()
        # self.oso.save('oso-measurement-inferred.ttl', overwrite=True)
        # ...and to the sqlite3 database.
        # world.save()


        # Manually change url of EMMO to `emmo_url` when importing it to make
        # it resolvable without consulting the catalog file.  This makes it possible
        # to open the ontology from url in Protege

        import rdflib  # noqa: E402, F401
        g = rdflib.Graph()
        g.parse(oso_measurement_filename , format=format)
        for s, p, o in g.triples(
                (None, rdflib.URIRef('http://www.w3.org/2002/07/owl#imports'), None)):
            if 'emmo-inferred' in o:
                g.remove((s, p, o))
                g.add((s, p, rdflib.URIRef(self.emmo_url)))
        g.serialize(destination=oso_measurement_filename, format=format)


if __name__ == '__main__':

    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)

    osm = OSO_Measurement()
    osm.define_ontology()
    osm.save_ontology()
    osm.save_ontology(format='xml')

    exit(0)

